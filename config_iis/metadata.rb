name             'config_iis'
maintainer       'Znalytics'
maintainer_email 'avishwanath@znalytics.com'
license          'All rights reserved'
description      'Installs/Configures config_iis'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
