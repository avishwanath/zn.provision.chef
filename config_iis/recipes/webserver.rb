powershell_script 'Install IIS' do
  code <<-EOH
  Import-Module ServerManager
  Add-WindowsFeature Web-Server
  EOH
  guard_interpreter :powershell_script
  not_if "Import-Module ServerManager; (Get-WindowsFeature -Name Web-Server).Installed"
end


powershell_script 'Install Web-Common-Http' do
  code 'Add-WindowsFeature Web-Common-Http'
  guard_interpreter :powershell_script
  not_if "$WebHttpState = (Get-WindowsFeature  File-Services).InstallState 
		 If ($WebHttpState -eq 'Available')
		 {
		  	 echo $false
		 }
		 Elseif ($WebHttpState -eq 'Installed')
		 {
			 echo $true
		 }"
end

powershell_script 'Install Web-Http-Logging' do
  code 'Add-WindowsFeature Web-Http-Logging'
  guard_interpreter :powershell_script
  not_if "$WebHttpLogState = (Get-WindowsFeature  Web-Http-Logging).InstallState 
		 If ($WebHttpLogState -eq 'Available')
		 {
		  	 echo $false
		 }
		 Elseif ($WebHttpLogState -eq 'Installed')
		 {
			 echo $true
		 }"
end

powershell_script 'Install Web-Http-Redirect' do
  code 'Add-WindowsFeature Web-Http-Redirect'
  guard_interpreter :powershell_script
  not_if "$WebHttpRedState = (Get-WindowsFeature  Web-Http-Redirect).InstallState 
		 If ($WebHttpRedState -eq 'Available')
		 {
		  	 echo $false
		 }
		 Elseif ($WebHttpRedState -eq 'Installed')
		 {
			 echo $true
		 }"
end

powershell_script 'Install Web-Custom-Logging' do
  code 'Add-WindowsFeature Web-Custom-Logging'
  guard_interpreter :powershell_script
  not_if "$WebCustomState = (Get-WindowsFeature  Web-Custom-Logging).InstallState 
		 If ($WebCustomState -eq 'Available')
		 {
		  	 echo $false
		 }
		 Elseif ($WebCustomState -eq 'Installed')
		 {
			 echo $true
		 }"
end

powershell_script 'Install Web-Log-Libraries ' do
  code 'Add-WindowsFeature Web-Log-Libraries '
  guard_interpreter :powershell_script
  not_if "$WebLogState = (Get-WindowsFeature  Web-Log-Libraries ).InstallState 
		 If ($WebLogState -eq 'Available')
		 {
		  	 echo $false
		 }
		 Elseif ($WebLogState -eq 'Installed')
		 {
			 echo $true
		 }"
end

powershell_script 'Install Web-Dyn-Compression' do
  code 'Add-WindowsFeature Web-Dyn-Compression'
  guard_interpreter :powershell_script
  not_if "$WebDynState = (Get-WindowsFeature  Web-Dyn-Compression ).InstallState 
		 If ($WebDynState -eq 'Available')
		 {
		  	 echo $false
		 }
		 Elseif ($WebDynState -eq 'Installed')
		 {
			 echo $true
		 }"
end

powershell_script 'Install Web-Basic-Auth' do
  code 'Add-WindowsFeature Web-Basic-Auth'
  guard_interpreter :powershell_script
  not_if "$WebBasicAuthState = (Get-WindowsFeature  Web-Basic-Auth ).InstallState 
		 If ($WebBasicAuthState -eq 'Available')
		 {
		  	 echo $false
		 }
		 Elseif ($WebBasicAuthState -eq 'Installed')
		 {
			 echo $true
		 }"
end

powershell_script 'Install Web-Windows-Auth ' do
  code 'Add-WindowsFeature Web-Windows-Auth '
  guard_interpreter :powershell_script
  not_if "$WebWinAuthState = (Get-WindowsFeature  Web-Windows-Auth  ).InstallState 
		 If ($WebWinAuthState -eq 'Available')
		 {
		  	 echo $false
		 }
		 Elseif ($WebWinAuthState -eq 'Installed')
		 {
			 echo $true
		 }"
end

powershell_script 'Install Web-App-Dev  ' do
  code 'Add-WindowsFeature Web-App-Dev  '
  guard_interpreter :powershell_script
  not_if "$WebAppDevState = (Get-WindowsFeature  Web-App-Dev ).InstallState 
		 If ($WebAppDevState -eq 'Available')
		 {
		  	 echo $false
		 }
		 Elseif ($WebAppDevState -eq 'Installed')
		 {
			 echo $true
		 }"
end

powershell_script 'Install Web-Net-Ext ' do
  code 'Add-WindowsFeature Web-Net-Ext '
  guard_interpreter :powershell_script
  not_if "$WebNetExt3.5State = (Get-WindowsFeature  Web-Net-Ext ).InstallState 
		 If ($WebNetExt3.5State -eq 'Available')
		 {
		  	 echo $false
		 }
		 Elseif ($WebNetExt3.5State -eq 'Installed')
		 {
			 echo $true
		 }"
end

powershell_script 'Install Web-Net-Ext45 ' do
  code 'Add-WindowsFeature Web-Net-Ext45 '
  guard_interpreter :powershell_script
  not_if "$WebNetExt4.5State = (Get-WindowsFeature  Web-Net-Ext45 ).InstallState 
		 If ($WebNetExt4.5State -eq 'Available')
		 {
		  	 echo $false
		 }
		 Elseif ($WebNetExt4.5State -eq 'Installed')
		 {
			 echo $true
		 }"
end

powershell_script 'Install Web-AppInit ' do
  code 'Add-WindowsFeature Web-AppInit '
  guard_interpreter :powershell_script
  not_if "$WebAppInitState = (Get-WindowsFeature  Web-AppInit ).InstallState 
		 If ($WebAppInitState -eq 'Available')
		 {
		  	 echo $false
		 }
		 Elseif ($WebAppInitState -eq 'Installed')
		 {
			 echo $true
		 }"
end

powershell_script 'Install Web-ASP ' do
  code 'Add-WindowsFeature Web-ASP '
  guard_interpreter :powershell_script
  not_if "$WebAppASPState = (Get-WindowsFeature  Web-ASP ).InstallState 
		 If ($WebAppASPState -eq 'Available')
		 {
		  	 echo $false
		 }
		 Elseif ($WebAppASPState -eq 'Installed')
		 {
			 echo $true
		 }"
end

powershell_script 'Install Web-Asp-Net45 ' do
  code 'Add-WindowsFeature Web-Asp-Net45 '
  guard_interpreter :powershell_script
  not_if "$WebASPNETState = (Get-WindowsFeature  Web-Asp-Net45 ).InstallState 
		 If ($WebASPNETState -eq 'Available')
		 {
		  	 echo $false
		 }
		 Elseif ($WebASPNETState -eq 'Installed')
		 {
			 echo $true
		 }"
end

powershell_script 'Install Web-CGI ' do
  code 'Add-WindowsFeature Web-CGI '
  guard_interpreter :powershell_script
  not_if "$WebCGIState = (Get-WindowsFeature  Web-CGI ).InstallState 
		 If ($WebCGIState -eq 'Available')
		 {
		  	 echo $false
		 }
		 Elseif ($WebCGIState -eq 'Installed')
		 {
			 echo $true
		 }"
end

powershell_script 'Install Web-ISAPI-Ext ' do
  code 'Add-WindowsFeature Web-ISAPI-Ext '
  guard_interpreter :powershell_script
  not_if "$WebISAPIEXTState = (Get-WindowsFeature  Web-ISAPI-Ext ).InstallState 
		 If ($WebISAPIEXTState -eq 'Available')
		 {
		  	 echo $false
		 }
		 Elseif ($WebISAPIEXTState -eq 'Installed')
		 {
			 echo $true
		 }"
end

powershell_script 'Install Web-ISAPI-Filter ' do
  code 'Add-WindowsFeature Web-ISAPI-Filter '
  guard_interpreter :powershell_script
  not_if "$WebISAPIFILState = (Get-WindowsFeature  Web-ISAPI-Filter ).InstallState 
		 If ($WebISAPIFILState -eq 'Available')
		 {
		  	 echo $false
		 }
		 Elseif ($WebISAPIFILState -eq 'Installed')
		 {
			 echo $true
		 }"
end

powershell_script 'Install Web-Includes ' do
  code 'Add-WindowsFeature Web-Includes '
  guard_interpreter :powershell_script
  not_if "$WebIncludeState = (Get-WindowsFeature  Web-Includes ).InstallState 
		 If ($WebIncludeState -eq 'Available')
		 {
		  	 echo $false
		 }
		 Elseif ($WebIncludeState -eq 'Installed')
		 {
			 echo $true
		 }"
end

powershell_script 'Install Web-WebSockets ' do
  code 'Add-WindowsFeature Web-WebSockets '
  guard_interpreter :powershell_script
  not_if "$WebSocketsState = (Get-WindowsFeature  Web-WebSockets).InstallState 
		 If ($WebSocketsState -eq 'Available')
		 {
		  	 echo $false
		 }
		 Elseif ($WebSocketsState -eq 'Installed')
		 {
			 echo $true
		 }"
end


