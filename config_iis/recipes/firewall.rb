netsh_firewall_rule 'World Wide Web Services (HTTPS Traffic-In)' do
  description 'Allow remote management over SSL'
  localport '443'
  action :enable
end