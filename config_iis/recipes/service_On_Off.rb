service 'OctopusDeploy Tentacle' do
  supports :status => true, :restart => true, :reload => true
  action [ :enable, :start ]
end

service "tzautoupdate" do
  action :disable
end

service "Browser" do
  action :disable
end

service "AppVClient" do
  action :disable
end

service "NetTcpPortSharing" do
  action :disable
end

service "CscService" do
  action :disable
end

service "Power" do
  action :disable
end

service "Spooler" do
  action :disable
end

service "RemoteAccess" do
  action :disable
end

service "SCardSvr" do
  action :disable
end

service "UevAgentService" do
  action :disable
end

service "WSearch" do
  action :disable
end

service "Themes" do
  action :disable
end

service "Audiosrv" do
  action :disable
end

service "MpsSvc" do
  action :disable
end